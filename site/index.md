# HYCAR

## Présentation des données et de l'enjeu

## Comportements auteurs

Extrait à venir. [Accéder à l'analyse complète]()

## Comportements de blogs multi-auteurs

Extrait à venir. [Accéder à l'analyse complète]()

---------

## Menu

Ce projet est open source, [fork me on gitlab](https://gitlab.com/open-scientist/hycar)

[Features](https://gitlab.com/open-scientist/hycar/-/blob/master/README.md#features)

[Architecture](https://gitlab.com/open-scientist/hycar/-/blob/master/README.md#architecture)

[Historique du développement](https://gitlab.com/open-scientist/hycar/-/blob/master/README.md#historique-du-développement)

[Comment contribuer](https://gitlab.com/open-scientist/hycar/-/blob/master/README.md#comment-contribuer)